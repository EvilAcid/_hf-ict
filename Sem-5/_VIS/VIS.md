# Virtuelle Infrastrucktur & Systeme

>

## Glossar

* IaaS -> Infastructure as a Service
* PaaS -> Platform as a Service
* SaaS -> Software as a Service
* 



## Projektarbeit

> Aufgabe, Eckdaten

Projekt von der HF-ICT

Schwerpunkt auf Blendet Learning , aufbau auf NextCloud(Share)

## Eckdaten

- 70 Lektionen
- ca 60-100h Aufwand pro Study

### Projektdurchführung von A-Z

- Evaluierung und Konzeption
- Planung
- Umsetzung
- Inbetriebnahme
- Präsentation und Dokumentation

### Bewertung / Benotung

- 4 Modulnoten
- Konzeption des Cloud-Projekts
- Abschlusspräsentation und Dokumentation des Cloud-Projektes
- Cloud-Lösung anhand der Abnahmekriterien

### Bewertung

#### Konzept (50%)

#### Wissensprüfung (50%)

