# SE

> Prüfung1 17.9.18

## Inhalt Prüfung 1

> Laptop als Datensichtgerät verwendbar, nicht zum nachprogrammieren

Verständnissfragen:

* Unterschied Maschinencode und Bytecode

* jvm auf fast allen systemen, platformunabhängig

* De-Faktor-Standard

* Java GNU General Publich License

* java.io, javafx

* javac <file.java>, java <name>

* Disassembler javap -c <file.class>

* Datentypen streng definiert

* package... Class not found, symbol not found <-- wird kommen :-)

* CLASSPATH="" export CLASSPATH

* Vorsicht mit Umgebungsvariabeln, werden gerne vergessen

* jar cvf myapp.jar <Package> zum packen

* packen mit manifest

* ausführbares Java

* *ANT kommt nicht*

*  Exeptions wird kommen

* checkt und uncheckt exeptions

* try catch finaly

* Threads, vorsicher beim selber erstellen von deadhlocks rais contitions etc. mit Synchronised..

* Race Conditions, 2 Threads greiffen gleichzeitig auf auf das gleiche objekt zu, kann zu Problemen Führen

* Synchronized schützt, das immer nur ein thread in einem Obj. sein kann

* Collections <Point> etc

* Rangebased for schleiffe

* Map, equals() und hashCode() kerrekt implementieren

* Gleichheit und Identität

* IO Stream, verschachtlung

* EventHandler setOnAction --> void handle (ActionEvent e)...
  Eventausgelöst, handle aufgerufen, procedur wird abgelaufen

  - ca. 10 Zeilen selber schreiben

  Vorgeschreibenes Programm fertigstellen

* 