package com.evilacid.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MathTest {
	Math ma = new Math();
	
	@Test
	void addition() {
		assertEquals(2000, ma.addition(1000, 1000), "1000 + 1000 Should give 2000");
	}
	
	@Test
	void subtraction() {
		assertEquals(0, ma.subtraction(100, 100),"100-100 should give 0");
	}
	

}
