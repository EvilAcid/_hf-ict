package com.evilacid.math;


public class Statistics {
private double [] numbers = null;
private int last=0;

public Statistics(int size) {
	numbers = new double[size];
}

public void addNumber(double n) {
	numbers[last++] = n;
}

public void getAverage() {
	double myAverage=0;
	for (double i : numbers) {
		myAverage += i;
		System.out.println(i);
	}
	System.out.println("The Average ist: " + myAverage/numbers.length);
}

public void getMaxValue() {
	double maxValue=0;
	for (double i : numbers) {
		if (maxValue < i) {
			maxValue = i;
		}
	}
	System.out.println("The Max-Value is: " + maxValue);
}

public void getMinValue() {
	double minValue=numbers[0];
	for (double i : numbers) {
		if (minValue > i) {
			minValue = i;
		}
	}
	System.out.println("The Min-Value is: " + minValue);
}
}
