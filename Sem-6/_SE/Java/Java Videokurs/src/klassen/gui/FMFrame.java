package klassen.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

public class FMFrame extends JFrame {

	 private JTable table;
	 private JScrollPane scrollTable;
	 private JProgressBar progBar;
	 private JButton btnInfo, btnAdd;
	 private JLabel lblHeader, lblHersteller, lblLeistung, lblPreis, lblTyp;
	 private JTextField fldHersteller;
	 private JSpinner spinLeistung, spinPreis;
	 private JComboBox boxType;
	 private JPanel pnlAdd;
	 private JPanel pnlLeft;


	 public FMFrame() {

		  setTitle("TestGUI");
		  // Programm beenden durch dr�cken des X Buttons
		  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		  createWidgets();
		  setupInteractions();
		  addWidgets();

		  // setSize(500, 500); // Wird mit dem Layooutmanager nicht mehr gemacht
		  pack(); // Dazu wird das Layout selbst berechnet f�r die Gr�sse

		  // Zentrieren auf dem Bildschirm
		  setLocationRelativeTo(null);
	 }

	 private void setupInteractions() {
		  btnAdd.addActionListener(new AddFahrzeugAction());

	 }


	 private void addWidgets() {
		  getContentPane().setLayout(new BorderLayout(5, 5));
		  getContentPane().add(BorderLayout.PAGE_START, lblHeader);
		  getContentPane().add(BorderLayout.PAGE_END, progBar);
		  getContentPane().add(BorderLayout.CENTER, scrollTable);
		  getContentPane().add(BorderLayout.LINE_START, pnlLeft);
		  // --
		  pnlAdd.add(lblHersteller);
		  pnlAdd.add(fldHersteller);

		  pnlAdd.add(lblLeistung);
		  pnlAdd.add(spinLeistung);

		  pnlAdd.add(lblPreis);
		  pnlAdd.add(spinPreis);

		  pnlAdd.add(lblTyp);
		  pnlAdd.add(boxType);
		  // --
		  pnlAdd.add(Box.createHorizontalGlue());
		  pnlAdd.add(btnAdd);

		  pnlAdd.setMaximumSize(pnlAdd.getPreferredSize());
		  pnlAdd.setAlignmentX(LEFT_ALIGNMENT);

		  pnlLeft.add(pnlAdd);
		  pnlLeft.add(Box.createVerticalGlue());
		  pnlLeft.add(btnInfo);
		  pnlLeft.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));


	 }


	 private void createWidgets() {
		  lblHeader = new JLabel("Fahrzeug Manager");
		  lblHeader.setFont(lblHeader.getFont().deriveFont(Font.BOLD + Font.ITALIC, 30));
		  lblHeader.setForeground(Color.WHITE);
		  lblHeader.setOpaque(true);
		  lblHeader.setBackground(Color.BLACK);
		  lblHeader.setHorizontalAlignment(SwingConstants.CENTER);
		  // -----------------------------------------------------------------------------------
		  progBar = new JProgressBar(0, 100);
		  progBar.setPreferredSize(new Dimension(0, 30));
		  // -----------------------------------------------------------------------------------
		  table = new JTable(100, 4);
		  scrollTable = new JScrollPane(table);
		  // -----------------------------------------------------------------------------------
		  btnInfo = new JButton("Information...");
		  // -----------------------------------------------------------------------------------
		  pnlAdd = new JPanel();
		  pnlAdd.setLayout(new GridLayout(0, 2, 5, 5));
		  // -----
		  lblHersteller = new JLabel("Hersteller");
		  lblLeistung = new JLabel("Leistung");
		  lblPreis = new JLabel("Preis");
		  lblTyp = new JLabel("Typ");

		  // -----------------------------------------------------------------------------------
		  fldHersteller = new JTextField();
		  spinLeistung = new JSpinner(new SpinnerNumberModel(100, 5, 1000, 10));
		  spinPreis = new JSpinner(new SpinnerNumberModel(5000, 500, 100000, 100));
		  boxType = new JComboBox(new Object[] { "PKW", "LKW", "Motorrad" });

		  // -----
		  btnAdd = new JButton("Hinzuf�gen");
		  // -----
		  pnlLeft = new JPanel();
		  pnlLeft.setLayout(new BoxLayout(pnlLeft, BoxLayout.PAGE_AXIS));

	 }
}
