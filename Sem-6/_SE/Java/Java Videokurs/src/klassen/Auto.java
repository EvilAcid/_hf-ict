package klassen;

public class Auto {

	// Instanz-Variablen (Attribute, Member)

	private int leistung; // PS
	private String hersteller; // Hersteller des Autos
	public static int erstellteAutos = 0;

	// Instanz-Methoden

	public void tunen(int tuneWert) {
		setLeistung(getLeistung() + tuneWert);
	}

	// Konstruktoren

	public Auto(int leistung, String hersteller) {
		this.setLeistung(leistung);
		this.setHersteller(hersteller);
		erstellteAutos += 1;
		System.out.println("Es wurde das " + erstellteAutos + " Auto erstellt.");
	}

	// Getter und Setter

	public String getHersteller() {
		return hersteller;
	}

	private void setHersteller(String hersteller) {
		this.hersteller = hersteller;
	}

	public int getLeistung() {
		return leistung;
	}

	public void setLeistung(int leistung) {
		/*
		 * TODO Fehlerhandling noch richtigstellen
		 * 
		 */
		if (leistung <= 0) {
			leistung = this.leistung;
		}
		this.leistung = leistung;
	}

	public static void printInfos(Auto auto) {
		System.out.println("Der Hersteller ist: " + auto.hersteller + " Leistung --> " + auto.leistung);

	}

}
