package programme;

import klassen.Auto;

class AutoProgramm {

	public static void main(String[] args) {

		Auto Skoda = new Auto(250, "Skoda");
		// Auto Golf = new Auto(98,"VW");

		// Aufruf f�r die Static Methode INFO
		Auto.printInfos(Skoda);

		Skoda.tunen(300);

		Auto.printInfos(Skoda);

	}

}
