package programme;

public class ArrayDemo {
	
	public static void main(String[] args) {
		
		int[] intArray = new int[3];
		intArray[0] = 01;
		intArray[1] = 10;
		intArray[2] = 20;
		
		for (int i : intArray) {
			System.out.println(i);
		}
	}

}
