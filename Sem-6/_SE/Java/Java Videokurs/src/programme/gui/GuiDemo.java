package programme.gui;

import javax.swing.JFrame;
import javax.swing.UIManager;

import klassen.gui.FMFrame;

public class GuiDemo {
	 public static void main(String[] args) {

		  try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		  } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		  }

		  JFrame frame = new FMFrame();

		  //frame.setResizable(false);
		  frame.setVisible(true);
	 }


}
