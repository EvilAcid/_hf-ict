﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoutPlaner
{
    class City
    {
        public WayPoint location { get; private set; }
        public string Name { get; private set; }
        public int Population { get; private set; }
        public string Country { get; private set; }

        public City(string name, double lat, double lon, int pop, string country)
        {
            Name = name;
            location = new WayPoint(name, lat, lon);
            Population = pop;
            Country = country;
        }

        public override string ToString()
        {
            double lonmin = (location.Longitude - (int)location.Longitude) * 60;
            double latmin = (location.Latitude - (int)location.Latitude) * 60;
            return string.Format(
                "WayPoint: {0} {1}° {2:##}' / {3}° {4:##}'",
                Name,
                (int)location.Latitude, latmin,
                (int)location.Longitude, lonmin);
        }
    }
}
