﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_test
{
    class Program
    {
        delegate void Notifire(string sender);
        static event Notifire greetings;

        public static void Sayhello(string sender)
        {
            Console.WriteLine("Hello from: " + sender);
            throw new NotImplementedException();
        }

        static void Main(string[] args)
        {
                       
            greetings += Sayhello;
            greetings("Test");
    
            Console.Read();
        }
    }
}
